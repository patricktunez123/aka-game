import React from "react";
import Square from "../components/Square";

const PlayGround = () => {
  return (
    <div className="akagame-container">
      <Square />
    </div>
  );
};

export default PlayGround;
