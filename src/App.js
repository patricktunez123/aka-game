import PlayGround from "./views/playGround";

function App() {
  return (
    <div className="akagame-container">
      <PlayGround />
    </div>
  );
}

export default App;
