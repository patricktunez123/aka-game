import React from "react";
import { moveSquare } from "../helpers";
import "./Square.scss";

const Square = () => {
  return (
    <div id="playGound" className="square-container">
      <div id="square" onClick={() => moveSquare()} className="square">
        Click me!
      </div>
    </div>
  );
};

export default Square;
