export const moveSquare = () => {
  let square = document.querySelector("#square");
  let playGound = document.querySelector("#playGound");
  let backgroundColor = "#";
  let clientHeight = playGound.clientHeight;
  let clientWidth = playGound.clientWidth;

  backgroundColor += Math.random().toString(16).slice(2, 8);

  square.style.position = "absolute";
  square.style.background = backgroundColor;
  square.style.bottom = parseInt(clientHeight * Math.random()) / 10 + "%";
  square.style.left = parseInt(clientWidth * Math.random()) / 20 + "%";

  playGound.appendChild(square);
};
